FROM ubuntu

COPY fdmd /fdmd
#COPY upload /upload
COPY config /config
COPY static /static

EXPOSE 3000
RUN chmod +x /fdmd
CMD ["/fdmd"]
