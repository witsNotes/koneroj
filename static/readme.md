---
title: "Readme"
author: "Jezri Krinsky"
---

# Markdown referenceing

## Pandoc markdown

All of the basic syntax is the pandoc dialect of markdown.
A full guide to the features of markdown can be found at [pandoc markdown](https://pandoc.org/MANUAL.html#pandocs-markdown)

## Referencing

Underscores around a heading make it reference another heading of the 
```{markdown}

```
## Markdown referencing


