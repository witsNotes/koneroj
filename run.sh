#!/bin/bash
docker kill web
docker rm web
docker run -d -P  --name web -p 3000:3000 --mount type=bind,source="$(pwd)"/upload,target=/upload fdmd:latest
echo "$(docker-machine ip default)":3000
